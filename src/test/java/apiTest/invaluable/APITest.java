// Need to fix RequestApprovalResult and AbsenteeBidCheck
package apiTest.invaluable;

import apiTest.invaluable.ResponseValidationBuilder;

import org.testng.annotations.Test;
import org.testng.Assert;

import java.util.Arrays;

import org.testng.annotations.DataProvider;
import org.testng.collections.Lists;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.empty;

import java.util.Iterator;


public class APITest {
  public String hostName = "stageweb.invaluable.com";
  public String baseHost = "http://stage.invaluable.com";
  public String subCategories = "AF14022FJQ";
  public String lotRef = "85138815";
  public String bidAmount = "30";
  public String catalogRef = "YJWDZYQ9JP";
  public String email = "api_test5@invaluable.com";
  public String password = "Password1";
  public String azToken = "9CB2F71D-1842-457B-B3E1-E9165C91477E";
  public String lefigueroToken = "9E9345BE-B648-4FCB-9425-ED59EB4B5593";
  
  public ResponseSpecification getResponseSpec(Boolean isLoggedIn) {
	ResponseSpecBuilder builder = new ResponseSpecBuilder();
	builder.expectStatusCode(200);
	builder.expectBody("pageAccessException", nullValue());
	builder.expectBody("errors", empty());
	builder.expectBody("data.isLoggedIn", equalTo(isLoggedIn));
	builder.expectBody("data.baseURL", equalTo(baseHost));
	builder.expectBody("data.pageAccessible", equalTo(true));
	ResponseSpecification responseSpec = builder.build();
	return responseSpec;
  }
  
  
  @Test
  public void getSuperCategories() {
	get("http://" + hostName + "/app/supercategories").
    then().
		spec(getResponseSpec(false)).body(matchesJsonSchemaInClasspath("super-categories.json")).log().body();
  }
  
  
  // Could not use JsonPath Operator to get all categoryRefs with a lotCount > 0
  // From: http://jsonpath.herokuapp.com/?path=%24.store.book%5B?(@.price%20%3C%2010)%5D
  // Example also fails in this evaluator: http://jsonpath.curiousconcept.com/
  @DataProvider
  public Object[][] retrieveCategoryRefs() {
	  JsonPath jsonResponse =  get("http://" + hostName + "/app/supercategories").jsonPath();
	  Iterator<Object> iterator = jsonResponse.getList("data.superCategories").iterator();

	  java.util.List<Object[]> categoryRefs = Lists.newArrayList();

	  while (iterator.hasNext()) {
		  String category = iterator.next().toString();
		  String categoryRef = category.split("categoryRef=")[1].split(",")[0].toString();
		  String lotCount = category.split("lotCount=")[1].split(",")[0];
		  categoryRefs.addAll(Arrays.asList(addParam(categoryRef, lotCount)));
	  }
	  return categoryRefs.toArray(new Object[categoryRefs.size()][]);
  }
  
  @DataProvider(name = "emailValues")
  public Object[][] emailValues() {
	  return new Object[][] { { "api_test@invaluable.com", "errors.validationErrors.email[0] KEYEQUALS That email address is already in useNEXTKV" }, {"doesnotexist@invaluable.com", null}};
  }
  
  
  @Test(dataProvider = "categoryRefs")
  public void getLotsByCategory(String categoryRef, String lotCount) {
	JsonPath jsonResponse = get("http://" + hostName + "/app/category/lots/" + categoryRef + "?displayNum=-1").jsonPath();
	Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), false);
	Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	Assert.assertEquals(jsonResponse.get("httpStatus"), 200);   
	Assert.assertEquals(jsonResponse.getList("data.lots").size(), Integer.parseInt(lotCount));
  }
  
  
  @Test(dataProvider = "emailValues")
  public void getEmailExists(String email, String errors) {
	ResponseSpecification responseSpec = new ResponseValidationBuilder.Builder().setIsLoggedIn(false).setErrors(errors).build();
	Integer x;
	x = 0;
	get("http://" + hostName + "/app/validate/emailAddressExists/" + email).
    then().
		spec(responseSpec); 
    x += 1;
  }  
  

  public String getAzToken() {
	  String aztoken = given().
			  cookie("AZTOKEN-STAGE", "9CB2F71D-1842-457B-B3E1-E9165C91477E").
			  get("http://stageweb.invaluable.com/app/member/login/" + email + "/" + password).
			  cookie("AZTOKEN-STAGE");
	  return aztoken;

  }
  
  Object[][] addParam(String param1, String param2) {
	  return new Object[][] {
		      new Object[] { param1, param2 },
	  };
  }	
  
 // POST tests
  @Test
  public void postLogin() {
	JsonPath jsonResponse = given().
	  relaxedHTTPSValidation().
      contentType("application/json").
      cookie("AZTOKEN-STAGE=9E9345BE-B648-4FCB-9425-ED59EB4B5593").
//      body("{\"userName\":\"" + email + "\",\"password\":\"" + password + "\"}").
      body("{\"userName\":\"api_test5@invaluable.com\",\"password\":\"Password1\"}").
    when().
      post("https://" + hostName + "/app/member/login").jsonPath();
	Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), true, "data.isLogged");
	Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	Assert.assertEquals(jsonResponse.get("httpStatus"), 200);
  }
  
//Needed to store each response value in a JsonPath object. 
//Could not figure out how to work around html tags that were returned in response.
 @Test
 public void postLogout() {
	  JsonPath jsonResponse = given().relaxedHTTPSValidation().
			  	cookie("AZTOKEN-STAGE", lefigueroToken).contentType("application/json").
			  when().
			  	post("https://" + hostName + "/app/member/logout").jsonPath();
	  Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), false);
	  Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	  Assert.assertEquals(jsonResponse.get("httpStatus"), 200);
 }  
  
 // @Test
  public void postResetPassword() {
	JsonPath jsonResponse = given().
      contentType("application/json").
      body("{\"userName\":\"api_test5@invaluable.com\",\"newPassword\":\"Password2\",\"oldPassword\":\"Password1\"}").
    when().
      post("http://" + hostName + "/app/member/submitResetPassword").jsonPath();
	Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), true);
	Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	Assert.assertEquals(jsonResponse.get("httpStatus"), 200);
  }

// Account created, but cannot login to UI with newly created user
//  Redirecting to /lefigaro/app/user/submitCreateAccount
  @Test
  public void postCreateAccount() {
	Response response = given().
	  relaxedHTTPSValidation().
      contentType("application/json").
      body("{\"userName\":\"api_test17@invaluable.com\",\"email\":\"api_test17@invaluable.com\",\"password\":\"Password1\",\"userPrefs\":{\"sendNewsLetter\":true}}").
    when().
      post("https://" + "stageweb.invaluable.com" + "/app/user/submitCreateAccount");
	System.out.println(response.getCookie("AZTOKEN-STAGE"));
	JsonPath jsonResponse = response.jsonPath();
	jsonResponse.prettyPrint();
    Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), true);
	Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	Assert.assertEquals(jsonResponse.get("httpStatus"), 200);
  }  
  
 
  @Test
  public void postAddCreditCard() {
	JsonPath jsonBeforeAdd =	given().
	  	cookie("AZTOKEN-STAGE", lefigueroToken).
    when().
        get("http://" + hostName + "/app/member/billing").jsonPath();
	JsonPath jsonResponse = given().
				  relaxedHTTPSValidation().
			      contentType("application/json").
			      cookie("AZTOKEN-STAGE", lefigueroToken).
		body("{\"ccType\":\"AMEX\",\"ccNumber\":\"372837177578767\",\"ccCVV\":\"1234\",\"ccExpiryMonth\":\"04\",\"ccExpiryYear\":\"2016\",\"nameOnCard\":\"Michael L Kaplan\",\"ccGUID\":\"\",\"companyName\":\"\",\"ccBillingAddress\":{\"addressStreet1\":\"38 Everett Street\",\"addressStreet2\":\"\",\"addressCity\":\"Allston\",\"countryCode\":\"US\",\"stateCode\":\"MA\",\"postalCode\" :\"02134\",\"addressPhone\":\"617-746-9830\"}}").
	when().
		post("http://" + "stageweb.invaluable.com" + "/app/member/submitCreditCard").jsonPath();
	  Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), true);
	  jsonResponse.prettyPrint();
  }  
 
// Not currently working  
  @Test
  public void deleteCreditCard() {
	given().
				  relaxedHTTPSValidation().
			      contentType("application/json").
			      cookie("AZTOKEN-STAGE", lefigueroToken).
	when().delete("https://stageweb.invaluable.com/app/member/deleteCreditCard/bdd853fa-28c1-42e8-9aab-d9d0264bb535").then().log().body();
  }  
  
  /* Need Access to get messages
  @Test
  public void requestData() {
	  JsonPath jsonResponse = given().relaxedHTTPSValidation().
			  	cookie("AZTOKEN-STAGE", getAzToken()).contentType("application/json").
			  	body("{\"catalogRef\":\"5WMXKXIU0Z\",\"messageTypeID\":3, \"messageText\":\"This is a message about a catalog.\"}").
			  when().
			  post("http://" + hostName + "/requestForInfo").jsonPath();
	  Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), false);
	  Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	  Assert.assertEquals(jsonResponse.get("httpStatus"), 200);
 }  
*/

  @Test
  public void postAddToWatchedLots() {
	  JsonPath jsonResponse = given().relaxedHTTPSValidation().
			  	cookie("AZTOKEN-STAGE", getAzToken()).contentType("application/json").
			  when().
			  post("http://" + hostName + "/app/member/watch-lot/URF9IESAHL").jsonPath();
	  Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), true);
	  Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	  Assert.assertEquals(jsonResponse.get("httpStatus"), 200);
 }  
  
  @Test
  public void postForgottenPassword() {
	  JsonPath jsonResponse = given().
			  body("{\"email\":\"api_test@invaluable.com\"}").
			  contentType("application/json").
			  when().
			  post("http://" + hostName + "/app/member/forgottenPassword").jsonPath();
	  Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), false);
	  Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	  Assert.assertEquals(jsonResponse.get("httpStatus"), 200);
 } 
  
  @Test
  public void postAddEditContactInfo() {
	  JsonPath jsonResponse = given().relaxedHTTPSValidation().
			  	cookie("AZTOKEN-STAGE", getAzToken()).contentType("application/json").
			  	body("{\"firstName\":\"API5\",\"lastName\":\"Test\",\"userAddress\":{\"addressStreet1\":\"39 Everett Street\",\"addressStreet2\":\"\",\"addressCity\":\"Brighton\",\"stateCode\":\"MA\",\"postalCode\":\"02135\",\"countryCode\":\"US\",\"addressPhone\":\"617-746-9831\"}}").
			  when().
			  post("http://" + hostName + "/app/user/submitEditContactInfo ").jsonPath();
	  Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), true);
	  Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	  Assert.assertEquals(jsonResponse.get("httpStatus"), 200);
	  
	  given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
	  when().
	  	get("http://" + hostName + "/app/user/editContactInfo").
	  then().
	  	spec(getResponseSpec(true)).body("data.user.firstName",  equalTo("API5"));
 } 

 // @Test
  public void postEditAccountInfo() {
	  JsonPath jsonResponse = given().relaxedHTTPSValidation().
			  	cookie("AZTOKEN-STAGE", getAzToken()).
			  	contentType("application/json").
			  	body("{\"email\":\"api_test@invaluable.com\",\"password\":\"Password1\",\"userPrefs\":{\"sendNewsLetter\":false,\"sendNotificationEmails\":false,\"sendReminderEmails\":false}}").
			  when().
			  post("http://" + hostName + "/app/user/submitEditAccount").jsonPath();
	  Assert.assertEquals(jsonResponse.get("data.isLoggedIn"), true);
	  Assert.assertSame(jsonResponse.get("errors").toString(), "[]");
	  Assert.assertEquals(jsonResponse.get("httpStatus"), 200);
 }    
  
// GET tests
  //Require aztoken

  @Test
  public void getLogin() {
	given().
		cookie("AZTOKEN-STAGE", getAzToken()). 
    when().
        get("http://" + hostName + "/app/member/login/" + email + "/" + password).
    then().
    	spec(getResponseSpec(true));
  }
  
  @Test
  public void getBidsCurrent() {    
	given().
		cookie("AZTOKEN-STAGE", getAzToken()).   	
    when().
        get("http://" + hostName + "/app/member/bids/current").
    then().
    	spec(getResponseSpec(true));
  }
  
  @Test
  public void getBidsPast() {
	given().
		cookie("AZTOKEN-STAGE", getAzToken()). 
    when().
        get("http://" + hostName + "/app/member/bids/past").
    then().
    	spec(getResponseSpec(true));
  }
  
  @Test
  public void getWatchedLotsCurrent() {
	given().
		cookie("AZTOKEN-STAGE", getAzToken()).         
    expect().
    when().
        get("http://" + hostName + "/app/member/watched-lots/current").
    then().
		spec(getResponseSpec(true));
  }
  
  @Test
  public void getWatchedLotsPast() {
	given().
		cookie("AZTOKEN-STAGE", getAzToken()).        
    when().
        get("http://" + hostName + "/app/member/watched-lots/past").
    then().
    	spec(getResponseSpec(true));
  }
  
  @Test
  public void getAuctionRegistrationsCurrent() {   
	given().
		cookie("AZTOKEN-STAGE", getAzToken()). 
    when().
        get("http://" + hostName + "/app/member/auctions/current").
    then().
    	spec(getResponseSpec(true));;
  }
  
  @Test
  public void getAuctionRegistrationsPast() {     
	given().
		cookie("AZTOKEN-STAGE", getAzToken()). 
    when().
        get("http://" + hostName + "/app/member/auctions/past").
    then().
    	spec(getResponseSpec(true));;
  } 

  @Test
  public void getMessages() {
	  given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
	  when(). 
	  	get("http://" + hostName + "/app/member/messages/Inbox").
	  then().
		spec(getResponseSpec(true));;
  }

 /*
  @Test
  public void getAbsenteeBidResult() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/orderBidResult/" + "86FDA87813" + "/" + bidAmount);
  } 
  */
  
  @Test
  public void getRequestForInfoLot() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).    
    when().
        get("http://" + hostName + "/app/requestForInfo/lot/" + "86FDA87813").
    then().
    	spec(getResponseSpec(true));
  } 
  
  @Test
  public void getRequestForInfoCatalog() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    when().
        get("http://" + hostName + "/app/requestForInfo/catalog/" + "YJWDZYQ9JP").
    then().
    	spec(getResponseSpec(true));
  }  
  
  @Test
  public void getContactInfo() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/user/editContactInfo").
    then().
   		spec(getResponseSpec(true));  
	  
  } 

  @Test
  public void getAccountInfo() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    when().
        get("http://" + hostName + "/app/user/editAccount").
    then().
   		spec(getResponseSpec(true));
  } 
  
  @Test
  public void getBillingInfo() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/member/billing").
    then().
    	spec(getResponseSpec(true));
  } 
  
  @Test
  public void getCreditCardInfo() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/member/creditCard/%7B921ad464-e393-4891-b0cc-e592757d7655%7D").
    then().
    	spec(getResponseSpec(true));
  } 

  @Test
  public void getRequestInitApprovalCheckCatalogRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/requestForApprovalInit/" + catalogRef).
    then().
   		spec(getResponseSpec(true));
  }  
  
  @Test
  public void getRequestInitApprovalCheckLotRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    when().
        get("http://" + hostName + "/app/requestForApprovalInit/" + catalogRef + "/" + lotRef).
    then().
    	spec(getResponseSpec(true));   
  }  
  
  @Test
  public void getRequestApprovalAbsenteeBidCatalogRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/loginModal/URF9IESAHL").
    then().
    	spec(getResponseSpec(true));   
  }  
  
  @Test
  public void getRequestApprovalAbsenteeBidLotRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/loginModal/YJWDZYQ9JP/A569E40086").
    then().
    	spec(getResponseSpec(true));   
  }  
  
  @Test
  public void getRequestApprovalAbsenteeBidOrderBidAmount() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/loginModal/YJWDZYQ9JP/A569E40086/20").
    then().
    	spec(getResponseSpec(true));   
  }  
  
  @Test
  public void getRequestApprovalPageCatalogRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/requestForApproval/YJWDZYQ9JP").
    then().
    	spec(getResponseSpec(true));   
  }  
  
  @Test
  public void getRequestApprovalPageLotRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/requestForApproval/YJWDZYQ9JP/A569E40086").
    then().
    	spec(getResponseSpec(true));   
  }  

  @Test
  public void getRequestApprovalPageOrderBidAmount() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/requestForApproval/YJWDZYQ9JP/A569E40086/20").
    then().
    	spec(getResponseSpec(true));   
  }  
 
  /*
  @Test
  public void getRequestApprovalResult() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/requestForApprovalResult/YJWDZYQ9JP");   
  }  
  */

  @Test
  public void getInitialAbsenteeBidCheck() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
    when().
        get("http://" + hostName + "/app/orderBidInit/A569E40086/20").
    then().
    	spec(getResponseSpec(true)).log().body();   
  }  

 /*
  @Test
  public void getAbsenteeBidCheck() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/orderBidConfirm/A569E40086/20");   
  }  
  */
  
  // aztoken not required
  @Test
  public void getSortByTimeEndingSoonest() {
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=42").
    then().
    	spec(getResponseSpec(false));
  }
  
  @Test
  public void getSortByTimeEndingLatest() {
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=41").
    then().
    	spec(getResponseSpec(false));
  }
  
  @Test
  public void getSortByPriceHightoLow() {
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45").
    then().
    	spec(getResponseSpec(false));
  }
  
  @Test
  public void getSortByPriceLowtoHigh() {
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=46").
    then().
		spec(getResponseSpec(false));
  }
  
  @Test
  public void getSortByNumberOfBidsLowtoHigh() {
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=71").
    then().
		spec(getResponseSpec(false));
  }
  
  @Test
  public void getSortByNumberOfBidsHighToLow() {
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=72").
    then().
    	spec(getResponseSpec(false));
  }
  
  @Test
  public void getFilterBySuperCategoryIDs() { 
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=72&superCategoryID=1").
    then().
		spec(getResponseSpec(false));
  }
  
  @Test
  public void getFilterByCategoryIDs() {
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&categoryID=50,51,52,53,54,55").
    then().
		spec(getResponseSpec(false));
  }
  
  @Test
  public void getFilterByLotSuperCategoryID() {  
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&lotSuperCategoryID=1").
    then().
		spec(getResponseSpec(false));
  }
  
  @Test
  public void getFilterByLotCategoryID() {
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&lotCategoryID=50").
    then().
		spec(getResponseSpec(false));
  }
  
  @Test
  public void getFilterByLotSubCategoryID() {
    get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&lotSubCategoryID=550").
    then().
		spec(getResponseSpec(false));
  }

  @Test
  public void getFilterbyPriceLow() {  
	get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&lowPrice=100").
	then().
		spec(getResponseSpec(false));
  }
  
  @Test
  public void getFilterbyPriceHigh() {
	get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&highPrice=1000").
    then().
		spec(getResponseSpec(false));
  }
 
  
  @Test
  public void getSubCategories() { 
    get("http://" + hostName + "/app/subcategories/" + subCategories).
    then().
		spec(getResponseSpec(false)).body(matchesJsonSchemaInClasspath("sub-categories.json"));
  }
  
  
  @Test
  public void getCountries() {
    get("http://" + hostName + "/app/countries").
    then().
		spec(getResponseSpec(false));
  }
  
  @Test
  public void getStates() { 
    get("http://" + hostName + "/app/states?country=US").
    then().
		spec(getResponseSpec(false));
  }

  @Test
  public void getLot() {
    get("http://" + hostName + "/app/lot/E98FF2B684").
    then().
		spec(getResponseSpec(false));   
  }

  @Test
  public void getCatalog() {
    get("http://" + hostName + "/app/catalog/YJWDZYQ9JP").
    then().
		spec(getResponseSpec(false)); 
  }

  @Test
  public void getUpcomingAuctions() {
    get("http://" + hostName + "/app/online-auctions").
    then().
		spec(getResponseSpec(false)); 
  } 

  @Test
  public void getPostalCodeValidation() {
    get("http://" + hostName + "/app/validate/postalCode/02050/en-us").
    then().
		spec(getResponseSpec(false));   
  }  
  
  @Test
  public void getPhoneNumValidation() {
    get("http://" + hostName + "/app/validate/phoneNumber/7813456789").
    then().
		spec(getResponseSpec(false));   
  }  
  
}
