package API_Test.invaluable;

import java.net.*;
import java.io.*;

public class UrlRequests {
    public static void main(String[] args) throws Exception {
        URL yahoo = new URL("http://stageweb.invaluable.com//app/catalog/YJWDZYQ9JP");
        URLConnection yc = yahoo.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                yc.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) 
            System.out.println(inputLine);
        in.close();
    }
}