package apiTest.invaluable;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.StringContains.containsString;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.BooleanUtils;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.specification.ResponseSpecification;

public class ResponseValidationBuilder
{
	private Boolean isLoggedIn;
	private Integer statusCode;
	private String baseHost;
	private String errors;
	private String errors_dot;
	private Boolean isPageAccessible;
/*
	  public ResponseSpecification getResponseSpec(Boolean isLoggedIn) {
			ResponseSpecBuilder builder = new ResponseSpecBuilder();
			builder.expectStatusCode(200);
			builder.expectBody("pageAccessException", nullValue());
			builder.expectBody("errors", empty());
			builder.expectBody("data.isLoggedIn", equalTo(isLoggedIn));
			builder.expectBody("data.baseURL", equalTo(baseHost));
			builder.expectBody("data.pageAccessible", equalTo(true));
			ResponseSpecification responseSpec = builder.build();
			return responseSpec;
		  }	
	*/
	public ResponseSpecBuilder builder = new ResponseSpecBuilder();
			

    // private constructor to defer responsibility to Builder.
    private ResponseValidationBuilder()
    {
    }

    public boolean getisLoggedIn()
    {
        return this.isLoggedIn;
    }

    public void setIsLoggedIn(final boolean isLoggedIn)
    {
        builder.expectBody("data.isLoggedIn", equalTo(isLoggedIn));
        this.isLoggedIn = isLoggedIn;
    }
    
    public Integer getStatusCode()
    {
        return this.statusCode;
    }

    public void setStatusCode(final Integer statusCode)
    {
    	if (statusCode != null)
    	{
    		builder.expectStatusCode(statusCode);
    		this.statusCode = statusCode;
    	}
    	else{
    		builder.expectStatusCode(200);
    		this.statusCode = 200;
    	}
    }
    
    public String getBaseHost()
    {
        return this.baseHost;
    }

    public void setBaseHost(final String baseHost)
    {
    	if (baseHost != null)
    	{
    		builder.expectBody("data.baseURL", equalTo(baseHost));
    		this.baseHost = baseHost;
    	}
    	else{
    		builder.expectBody("data.baseURL", equalTo("http://stage.invaluable.com"));
    		this.baseHost = "http://stage.invaluable.com";
    	}
    }
    
    public String getErrors()
    {
        return this.errors;
    }

    public void setErrors(final String errors)
    {
    	if (errors == null)
    	{
    		builder.expectBody("errors", empty());
    		this.errors = "[]";
    	}
    	else{
    		for (String error_parts : errors.split("NEXTKV")) {
    			String[] key_value = error_parts.split("KEYEQUALS");
    			String key = key_value[0];
    			String value = key_value[1];
    			builder.expectBody(key, containsString(value));
    			this.errors = errors;
    		}

    	}
    }

    // builder class to ensure that all required fields are set while avoiding clunky, "telescopic" constructors
    public static class Builder
    {
        private ResponseValidationBuilder built;

        public Builder()
        {
            built = new ResponseValidationBuilder();
        }

        public Builder setIsLoggedIn(final boolean isLoggedIn)
        {
            built.setIsLoggedIn(isLoggedIn);
            return this;
        }
     
        public Builder setStatusCode(final Integer statusCode)
        {
            built.setStatusCode(statusCode);
            return this;
        }
        
        public Builder setBaseHost(final String baseHost)
        {
            built.setBaseHost(baseHost);
            return this;
        } 
        
        public Builder setErrors(final String errors)
        {
            built.setErrors(errors);
            return this;
        } 

        public ResponseSpecification build()
        {
        	ResponseSpecBuilder builder = new ResponseSpecBuilder();
            ResponseSpecification responseSpec = builder.build();
            return responseSpec;
        }
    }
}