// Need to fix RequestApprovalResult and AbsenteeBidCheck
package API_Test.invaluable;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.util.Arrays;

import org.testng.annotations.DataProvider;
import org.testng.collections.Lists;

import com.jayway.restassured.builder.ResponseSpecBuilder;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.specification.ResponseSpecification;

import static com.jayway.restassured.RestAssured.expect;
import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.empty;

import java.util.Iterator;


public class APITest {
  public String hostName = "stageweb.invaluable.com";
  public String baseHost = "http://stage.invaluable.com";
  public String subCategories = "AF14022FJQ";
  public String lotRef = "85138815";
  public String bidAmount = "20";
  public String catalogRef = "YJWDZYQ9JP";
  
  public ResponseSpecification getResponseSpec(Boolean isLoggedIn) {
	ResponseSpecBuilder builder = new ResponseSpecBuilder();
	builder.expectStatusCode(200);
	builder.expectBody("pageAccessException", nullValue());
	builder.expectBody("errors", empty());
	builder.expectBody("data.isLoggedIn", equalTo(isLoggedIn));
	builder.expectBody("data.baseURL", equalTo(baseHost));
	builder.expectBody("data.pageAccessible", equalTo(true));
	ResponseSpecification responseSpec = builder.build();
	return responseSpec;
  }

  public String getAzToken() {
	  String aztoken = given().
			  cookie("AZTOKEN-STAGE", "454D8955-6E64-49F9-B49E-B631F7E8F0A3	").
			  get("http://stageweb.invaluable.com/app/member/login/api_test@invaluable.com/Password1").
			  cookie("AZTOKEN-STAGE");
	  return aztoken;

  }
  
	public Object[][] addParam(String param1, String param2) {
		  return new Object[][] {
		      new Object[] { param1, param2 },
		  };
		}	
  
  // Could not use JsonPath Operator to get all categoryRefs with a lotCount > 0
  // From: http://jsonpath.herokuapp.com/?path=%24.store.book%5B?(@.price%20%3C%2010)%5D
  // Example also fails in this evaluator: http://jsonpath.curiousconcept.com/
  @DataProvider
  public Object[][] categoryRefs() {
	  JsonPath jsonResponse =  get("http://" + hostName + "/app/supercategories").jsonPath();
//	  Iterator<Object> iterator = jsonResponse.getList("data.superCategories.findAll { superCategories -> superCategories.lotCount > 1 }").iterator();
	  Iterator<Object> iterator = jsonResponse.getList("data.superCategories").iterator();

	  java.util.List<Object[]> categoryRefs = Lists.newArrayList();
	  while (iterator.hasNext()) {
		  String category = iterator.next().toString();
		  String categoryRef = category.split("categoryRef=")[1].split(",")[0].toString();
		  String lotCount = category.split("lotCount=")[1].split(",")[0].toString();
		  categoryRefs.addAll(Arrays.asList(addParam(categoryRef, lotCount)));
	  }
	  return categoryRefs.toArray(new Object[categoryRefs.size()][]);
  }
  
 // POST tests
  @Test
  public void Login_Post() {
	given().
	  relaxedHTTPSValidation().
      contentType("application/json").
      body("{\"userName\":\"api_test@invaluable.com\",\"password\":\"Password1\"}").
      cookie("AZTOKEN-STAGE=454D8955-6E64-49F9-B49E-B631F7E8F0A3").
    when().
      post("https://" + hostName + "/app/member/login").
    then().
      statusCode(200);
  }

// GET tests
  //Require aztoken
  @Test
  public void BidsCurrent() {    
	given().
		cookie("AZTOKEN-STAGE", getAzToken()).
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/member/bids/current");
  }
  
  @Test
  public void BidsPast() {
	given().
		cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/member/bids/past");
  }
  
  @Test
  public void WatchedLotsCurrent() {
	given().
		cookie("AZTOKEN-STAGE", getAzToken()).         
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/member/watched-lots/current");
  }
  
  @Test
  public void WatchedLotsPast() {
	given().
		cookie("AZTOKEN-STAGE", getAzToken()).        
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/member/watched-lots/past");
  }
  
  @Test
  public void AuctionRegistrationsCurrent() {   
	given().
		cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/member/auctions/current");
  }
  
  @Test
  public void AuctionRegistrationsPast() {     
	given().
		cookie("AZTOKEN-STAGE", getAzToken()). 
	expect().
		spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/member/auctions/past");
  } 

  @Test
  public void Messages() {
	  given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).
	  expect().
	  	spec(getResponseSpec(true)).
	  	body("data.isLoggedIn", equalTo(true)).
	  when(). 
	  	get("http://" + hostName + "/app/member/messages/Inbox");
  }

 /*
  @Test
  public void AbsenteeBidResult() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/orderBidResult/" + "86FDA87813" + "/" + bidAmount);
  } 
  */
  
  @Test
  public void RequestForInfoLot() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/requestForInfo/lot/" + "86FDA87813");
  } 
  
  @Test
  public void RequestForInfoCatalog() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/requestForInfo/catalog/" + "YJWDZYQ9JP");
  }  
  
  @Test
  public void GetContactInfo() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/user/editContactInfo");
  } 

  @Test
  public void GetAccountInfo() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/user/editAccount");
  } 
  
  @Test
  public void GetBillingInfo() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/member/billing ");
  } 
  
  @Test
  public void GetCreditCardInfo() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()).      
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/member/creditCard/%7B921ad464-e393-4891-b0cc-e592757d7655%7D");
  } 

  @Test
  public void RequestInitApprovalCheckCatalogRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/requestForApprovalInit/" + catalogRef);   
  }  
  
  @Test
  public void RequestInitApprovalCheckLotRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/requestForApprovalInit/" + catalogRef + "/" + lotRef);   
  }  
  
  @Test
  public void RequestApprovalAbsenteeBidCatalogRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/loginModal/URF9IESAHL");   
  }  
  
  @Test
  public void RequestApprovalAbsenteeBidLotRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/loginModal/YJWDZYQ9JP/A569E40086");   
  }  
  
  @Test
  public void RequestApprovalAbsenteeBidOrderBidAmount() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/loginModal/YJWDZYQ9JP/A569E40086/20");   
  }  
  
  @Test
  public void RequestApprovalPageCatalogRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/requestForApproval/YJWDZYQ9JP");   
  }  
  
  @Test
  public void RequestApprovalPageLotRef() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/requestForApproval/YJWDZYQ9JP/A569E40086");   
  }  

  @Test
  public void RequestApprovalPageOrderBidAmount() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/requestForApproval/YJWDZYQ9JP/A569E40086/20");   
  }  
 
  /*
  @Test
  public void RequestApprovalResult() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/requestForApprovalResult/YJWDZYQ9JP");   
  }  
  */

  @Test
  public void InitialAbsenteeBidCheck() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/orderBidInit/A569E40086/20");   
  }  

 /*
  @Test
  public void AbsenteeBidCheck() {
	given().
	  	cookie("AZTOKEN-STAGE", getAzToken()). 
    expect().
    	spec(getResponseSpec(true)).
    when().
        get("http://" + hostName + "/app/orderBidConfirm/A569E40086/20");   
  }  
  */
  
//Needed to store each response value in a JsonPath object. 
//Could not figure out how to work around html tags that were returned in response.
 @Test
 public void Logout() {
	  JsonPath jsonResponse = given().relaxedHTTPSValidation().
			  	cookie("AZTOKEN-STAGE", getAzToken()).contentType("application/json").
			  when().
			  get("https://" + hostName + "/app/member/logout").jsonPath();
	  AssertJUnit.assertEquals(jsonResponse.get("data.isLoggedIn"), false);
	  AssertJUnit.assertSame(jsonResponse.get("errors").toString(), "[]");
	  AssertJUnit.assertEquals(jsonResponse.get("httpStatus"), 200);
 }  
  
  // aztoken not required
  @Test
  public void SortByTimeEndingSoonest() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=42");
  }
  
  @Test
  public void SortByTimeEndingLatest() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=41");
  }
  
  @Test
  public void SortByPriceHightoLow() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45");
  }
  
  @Test
  public void SortByPriceLowtoHigh() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=46");
  }
  
  @Test
  public void SortByNumberOfBidsLowtoHigh() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=71");
  }
  
  @Test
  public void SortByNumberOfBidsHighToLow() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=72");
  }
  
  @Test
  public void FilterBySuperCategoryIDs() { 
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=72&superCategoryID=1");
  }
  
  @Test
  public void FilterByCategoryIDs() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&categoryID=50,51,52,53,54,55");
  }
  
  @Test
  public void FilterByLotSuperCategoryID() {  
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&lotSuperCategoryID=1");
  }
  
  @Test
  public void FilterByLotCategoryID() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&lotCategoryID=50");
  }
  
  @Test
  public void FilterByLotSubCategoryID() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&lotSubCategoryID=550");
  }

  @Test
  public void FilterbyPriceLow() {  
	  expect().
  		spec(getResponseSpec(false)).
	  when().
	  	get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&lowPrice=100");
  }
  
  @Test
  public void FilterbyPriceHigh() {
	  expect().
	  	  spec(getResponseSpec(false)).
	  when().
	  	get("http://" + hostName + "/app/search/lots/solr?query=coins&groupByDate=false&sortValue=45&highPrice=1000");
  }
  
  @Test
  public void GetSuperCategories() {
	  expect().
  		spec(getResponseSpec(false)).
	  when().
        get("http://" + hostName + "/app/supercategories");
  }
  
  @Test
  public void GetSubCategories() { 
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/subcategories/" + subCategories);
  }
  
  @Test(dataProvider = "categoryRefs")
  public void GetLotsByCategory(String categoryRef, String lotCount) {
    expect().
    	spec(getResponseSpec(false)).
    	body("data.lots", hasSize(Integer.parseInt(lotCount))).
    when().
        get("http://" + hostName + "/app/category/lots/" + categoryRef + "?displayNum=-1");
  }
  
  @Test
  public void GetCountries() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/countries");
  }
  
  @Test
  public void GetStates() { 
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/states?country=US");
  }

  @Test
  public void GetLot() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/lot/E98FF2B684");   
  }

  @Test
  public void GetCatalog() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/catalog/YJWDZYQ9JP"); 
  }

  @Test
  public void GetUpcomingAuctions() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/online-auctions"); 
  } 
  
  @Test
  public void EmailExists() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/validate/emailAddressExists/xxxx@invaluable.com");   
  }  

  @Test
  public void PostalCodeValidation() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/validate/postalCode/02050/en-us");   
  }  
  
  @Test
  public void PhoneNumValidation() {
    expect().
    	spec(getResponseSpec(false)).
    when().
        get("http://" + hostName + "/app/validate/phoneNumber/7813456789");   
  }  
  
}
